FROM alpine
RUN apk add curl
COPY ./entrypoint.sh /
ENTRYPOINT ["/entrypoint.sh"]