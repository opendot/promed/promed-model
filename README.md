This image retrieves the data model JSON file set in the [EGI DataHub](https://datahub.egi.eu/) repository for the project, and downloads it into a shared volume among all the other images.

A template of the JSON for the data model is like the following:

```:json
{
	"machine_model" : {
		"model_name": "SISMA MYSINT100",    
		"recoating_time" : 0.01,
		"sintering_time" : 0.01,
		"detaching_effort" : 12,
		"working_area" : {
			"x" : 200,
			"y" : 200,
			"z" : 200
		}
	}
}
```